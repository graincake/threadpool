#pragma once
#include "stddef.hpp"
#include "task_t.hpp"
#include "thread_t.h"
#include "task_queue_t.h"
class threadpool_t
{
public:
	typedef std::lock_guard<std::mutex> lock_guard_t;

	threadpool_t(void);
	~threadpool_t(void);
	void add_task(task_t &task_);
	void init(uint32_t max_thread_num, uint32_t idle_timeout);
	void destroy();
private:
	void init_thread(uint32_t max_thread_num, uint32_t idle_timeout);
	void create_a_new_thread();
	bool pull_task(task_ptr_t &task_);
	void thread_exit_pro(uint32_t tid_);
	uint32_t next_tid();
	typedef std::list<thread_ptr_t> thread_list_t;
	thread_t::pull_task_fun_t pull_task_fun;
	thread_t::exit_fun_t thread_exit_fun;
	thread_list_t alive_thread_list;
	thread_list_t dead_thread_list;
	task_queue_t task_queue;
	std::mutex thread_list_mutex;
	uint32_t mini_thread_count;
	uint32_t max_thread_num;
	uint32_t idle_timeout;
};
